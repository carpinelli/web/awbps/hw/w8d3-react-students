#!/bin/bash

# Removes project template's git folder
rm -rf .git/
# Setup Git for a new project
git init --initial-branch=main
repo_name=$(git rev-parse --show-toplevel | xargs basename).git
branch_name=$(git rev-parse --abbrev-ref HEAD)
git add .
git commit -m "Initial commit."
#git push --set-upstream git@gitlab.com:carpinelli/$repo_name $branch_name
#git remote add origin git@gitlab.example.com:namespace/$repo_name.git
#git remote add origin git@gitlab.com:carpinelli/$repo_name.git
