const Score = function({ score })
{
    return (
        <div className="score">
            <p className="score-date">Date: {score.date}</p>
            <p className="score-value">Score: {score.score}</p>
        </div>
    );
};


export default Score;
