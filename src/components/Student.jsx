import Score from "./Score";


const Student = function({ student })
{
    const scoreComponents = student.scores.map((score) =>
    {
        return (
            <Score
             key={[student.id, score.date, score.score].join("-")}
             score={score}
            />
        );
    });

    return (
        <div className="student">
            <p className="student-name">Name: {student.name}</p>
            <p className="student-bio">Bio: {student.bio}</p>
            {scoreComponents}
        </div>
    );
};


export default Student;
