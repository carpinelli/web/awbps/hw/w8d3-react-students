import { useState } from 'react'

import rawStudentData from "./students";
import Student from "./components/Student";
import './App.css'



function App() {
  const [studentData, setStudentData] = useState(rawStudentData);

  return (
    <>
      <ul className="students">
        {studentData.map((member) => <Student key={member.id} student={member} />)}
      </ul>
    </>
  );
}

export default App
